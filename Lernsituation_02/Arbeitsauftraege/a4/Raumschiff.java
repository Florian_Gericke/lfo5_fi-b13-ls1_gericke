import java.util.ArrayList;
import java.util.Collection;
import java.util.Random;

public class Raumschiff {
    private int photonentorpedoAnzahl,
            energieversorgungInProzent,
            schildeInProzent,
            huelleInProzent,
            lebenserhaltungssystemeInProzent,
            androidenAnzahl;
    private String schiffsname;
    private static ArrayList<String> broadcastKommunikator;
    private ArrayList<Ladung> ladungsverzeichnis;

    public Raumschiff() {
    }

    public Raumschiff(int photonentorpedoAnzahl,
                      int energieversorgungInProzent,
                      int schildeInProzent,
                      int huelleInProzent,
                      int lebenserhaltungssystemeInProzent,
                      int androidenAnzahl,
                      String schiffsname) {

        this.photonentorpedoAnzahl = photonentorpedoAnzahl;
        this.energieversorgungInProzent = energieversorgungInProzent;
        this.schildeInProzent = schildeInProzent;
        this.huelleInProzent = huelleInProzent;
        this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
        this.androidenAnzahl = androidenAnzahl;
        this.schiffsname = schiffsname;

        ladungsverzeichnis = new ArrayList<>();
        broadcastKommunikator = new ArrayList<>();
    }


    public int getPhotonentorpedoAnzahl() {
        return photonentorpedoAnzahl;
    }

    public void setPhotonentorpedoAnzahl(int photonentorpedoAnzahl) {
        this.photonentorpedoAnzahl = photonentorpedoAnzahl;
    }

    public int getEnergieversorgungInProzent() {
        return energieversorgungInProzent;
    }

    public void setEnergieversorgungInProzent(int energieversorgungInProzent) {
        this.energieversorgungInProzent = energieversorgungInProzent;
    }

    public int getSchildeInProzent() {
        return schildeInProzent;
    }

    public void setSchildeInProzent(int schildeInProzent) {
        this.schildeInProzent = schildeInProzent;
    }

    public int getHuelleInProzent() {
        return huelleInProzent;
    }

    public void setHuelleInProzent(int huelleInProzent) {
        this.huelleInProzent = huelleInProzent;
    }

    public int getLebenserhaltungssystemeInProzent() {
        return lebenserhaltungssystemeInProzent;
    }

    public void setLebenserhaltungssystemeInProzent(int lebenserhaltungssystemeInProzent) {
        this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
    }

    public int getAndroidenAnzahl() {
        return androidenAnzahl;
    }

    public void setAndroidenAnzahl(int androidenAnzahl) {
        this.androidenAnzahl = androidenAnzahl;
    }

    public String getSchiffsname() {
        return schiffsname;
    }

    public void setSchiffsname(String schiffsname) {
        this.schiffsname = schiffsname;
    }

    public void addLadung(Ladung neueLadung) {
        ladungsverzeichnis.add(neueLadung);
    }

    public void addLadung(Collection<Ladung> collection) {
        for (Ladung item : collection) {
            addLadung(item);
        }
    }

    public void photonentorpedoSchiessen(Raumschiff r) {
        if (photonentorpedoAnzahl > 0) {
            photonentorpedoAnzahl--;
            treffer(r);
            nachrichtAnAlle("Photonentorpedo abgeschossen");
        } else {
            nachrichtAnAlle("-=*Click*=-");
        }
    }

    public void phaserkanoneSchiessen(Raumschiff r) {
        nachrichtAnAlle(energieversorgungInProzent > 50 ? "Phaserkanone abgeschossen " : "-=*Click*=-");
        energieversorgungInProzent /= 2;
        treffer(r);
    }

    private void treffer(Raumschiff r) {
        System.out.println(r.getSchiffsname() + " wurde getroffen!");

        r.setSchildeInProzent(r.getSchildeInProzent() / 2);
        if (r.getSchildeInProzent() <= 0) {
            r.setHuelleInProzent(r.getHuelleInProzent() / 2);
            if (r.getHuelleInProzent() <= 0) {
                nachrichtAnAlle("Lebenserhaltungssysteme von " + r.getSchiffsname() + " vernichtet");
            }
        }
    }

    public void nachrichtAnAlle(String message) {
        System.out.println(message);
        broadcastKommunikator.add(message);
    }

    public static ArrayList<String> eintraegeLogbuchZurueckgeben() {
        return broadcastKommunikator;
    }


    public void reparaturDurchfuehren(boolean schutzschilde,
                                      boolean energieversorgung,
                                      boolean schiffshuelle,
                                      int anzahlDroiden) {

        int rnd = new Random().nextInt(100);

        if (schutzschilde) {
            setSchildeInProzent(((rnd * anzahlDroiden) / getSchildeInProzent()));
        }
        if (energieversorgung) {
            setEnergieversorgungInProzent(((rnd * anzahlDroiden) / getEnergieversorgungInProzent()));
        }
        if (schiffshuelle) {
            setHuelleInProzent(((rnd * anzahlDroiden) / getHuelleInProzent()));
        }

    }

    public void zustandRaumschiff() {
        System.out.println(this);
    }

    public void ladungsverzeichnisAusgeben() {
        System.out.println(ladungsverzeichnis);
    }

    public void photonentorpedosLaden(int anzahlTorpedos) {
        Ladung found = null;
        for (Ladung item : ladungsverzeichnis) {
            if (item.getBezeichnung().equals("PhotonenTorpedo"))
                found = item;
        }
        if (found == null) {
            nachrichtAnAlle("Keine Photonentorpedos gefunden!");
            nachrichtAnAlle("-=*Click*=-");
        }
        int amount = Math.min(anzahlTorpedos, found.getMenge());
        setPhotonentorpedoAnzahl(amount);
        found.setMenge(found.getMenge() - amount);
    }

    public void ladungsverzeichnisAufraeumen() {
        ladungsverzeichnis.removeIf(item -> item.getMenge() == 0);
    }

    @Override
    public String toString() {
        return "Raumschiff{" +
                "\n\tphotonentorpedoAnzahl=" + photonentorpedoAnzahl +
                "\n\tenergieversorgungInProzent=" + energieversorgungInProzent +
                "\n\tschildeInProzent=" + schildeInProzent +
                "\n\thuelleInProzent=" + huelleInProzent +
                "\n\tlebenserhaltungssystemeInProzent=" + lebenserhaltungssystemeInProzent +
                "\n\tandroidenAnzahl=" + androidenAnzahl +
                "\n\tschiffsname='" + schiffsname + '\'' +
                "\n\tbroadcastKommunikator=" + broadcastKommunikator +
                "\n\tladungsverzeichnis=" + ladungsverzeichnis +
                "\n}";
    }
}

import java.util.Arrays;

public class StarTrek {

    private static Raumschiff klingonen,
            romulaner,
            vulkanier;

    public static void main(String[] args) {
        anfangszustand();

        System.out.println("============================================================");
        System.out.println("=                       Start zustand                      =");
        System.out.println("============================================================");
        System.out.println(klingonen);
        System.out.println(romulaner);
        System.out.println(vulkanier);
        System.out.println("============================================================");
        System.out.println("=                        Siel                              =");
        System.out.println("============================================================");
        play();
        System.out.println("============================================================");
        System.out.println("=                       End  zustand                       =");
        System.out.println("============================================================");
        System.out.println(klingonen);
        System.out.println(romulaner);
        System.out.println(vulkanier);
        System.out.println("============================================================");
        System.out.println("=                     Broadcast channel                    =");
        System.out.println("============================================================");
        System.out.println(Raumschiff.eintraegeLogbuchZurueckgeben());

    }

    private static void anfangszustand() {
        klingonen = new Raumschiff(1,
                100,
                100,
                100,
                100,
                2,
                "IKS Hegh'ta");

        romulaner = new Raumschiff(2,
                100,
                100,
                100,
                100,
                2,
                "IRW Khazara");

        vulkanier = new Raumschiff(3,
                80,
                80,
                50,
                100,
                5,
                "Ni'Var");

        klingonen.addLadung(Arrays.asList(
                new Ladung("Ferengi Schneckensaft", 200),
                new Ladung("Bat'leth Klingonen Schwert", 200)
        ));

        romulaner.addLadung(Arrays.asList(
                new Ladung("Borg Schrott", 5),
                new Ladung("Rote Materie", 200),
                new Ladung("Plasma Waffe", 50)
        ));

        vulkanier.addLadung(Arrays.asList(
                new Ladung("Forschungssonde", 35),
                new Ladung("PhotonenTorpedo", 3)
        ));

    }

    private static void play() {
        /*
            Die Klingonen schießen mit dem Photonentorpedo einmal auf die Romulaner.

            Die Romulaner schießen mit der Phaserkanone zurück.

            Die Vulkanier senden eine Nachricht an Alle “Gewalt ist nicht logisch”.

            Die Klingonen rufen den Zustand Ihres Raumschiffes ab und geben Ihr Ladungsverzeichnis aus

            Die Vulkanier sind sehr sicherheitsbewusst und setzen alle Androiden zur Aufwertung ihres Schiffes ein
            .
            Die Vulkanier verladen Ihre Ladung “Photonentorpedos” in die Torpedoröhren Ihres Raumschiffes und räumen das Ladungsverzeichnis auf.
            Die Klingonen schießen mit zwei weiteren Photonentorpedo auf die Romulaner.
         */

        klingonen.photonentorpedoSchiessen(romulaner);
        romulaner.phaserkanoneSchiessen(klingonen);
        vulkanier.nachrichtAnAlle("Gewalt ist nicht logisch");
        klingonen.zustandRaumschiff();
        klingonen.ladungsverzeichnisAusgeben();

        vulkanier.reparaturDurchfuehren(true, true, true, vulkanier.getAndroidenAnzahl());

        vulkanier.photonentorpedosLaden(10);
        vulkanier.ladungsverzeichnisAufraeumen();
        klingonen.photonentorpedoSchiessen(romulaner);
        klingonen.photonentorpedoSchiessen(romulaner);


    }
}

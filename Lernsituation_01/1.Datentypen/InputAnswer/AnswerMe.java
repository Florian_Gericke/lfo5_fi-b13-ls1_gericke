import java.util.Scanner;

public class AnswerMe {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Wie heist du ?");
        String name = scan.next();
        System.out.println("Wie alt bist du ?");
        int age = scan.nextInt();
        System.out.printf("Hallo "+name+" du bist " +age+" Jahre alt \nIch freue mich dich kennen zu lernen");
    }
}
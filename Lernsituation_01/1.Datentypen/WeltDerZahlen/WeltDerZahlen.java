/**
 * Aufgabe:  Recherechieren Sie im Internet !
 * <p>
 * Sie dürfen nicht die Namen der Variablen verändern !!!
 * <p>
 * Vergessen Sie nicht den richtigen Datentyp !!
 *
 * @author << Florian Gericke >>
 * @version 1.0 from 21.08.2019
 */

public class WeltDerZahlen {

    public static void main(String[] args) {

    /*  *********************************************************

         Zuerst werden die Variablen mit den Werten festgelegt!

    *********************************************************** */
        // Im Internet gefunden ?
        // Die Anzahl der Planeten in unserem Sonnesystem
        byte anzahlPlaneten = 8;

        // Anzahl der Sterne in unserer Milchstraße
        long anzahlSterne = 400000000000L;

        // Wie viele Einwohner hat Berlin
        int bewohnerBerlin = 3664088;

        // Wie alt bist du?  Wie viele Tage sind das?

        int alterTage = 8481;

        // Wie viel wiegt das schwerste Tier der Welt?
        // Schreiben Sie das Gewicht in Kilogramm auf!
        byte gewichtKilogramm = 80;

        // Schreiben Sie auf, wie viele km² das größte Land er Erde hat?
        int flaecheGroessteLand = 17098242;

        // Wie groß ist das kleinste Land der Erde?
        byte flaecheKleinsteLand = 1;


/*  *********************************************************

     Programmieren Sie jetzt die Ausgaben der Werte in den Variablen

*********************************************************** */

        System.out.println("Anzhahl der Planeten:\t\t\t" + anzahlPlaneten);
        System.out.println("Anzhahl der Sterne:\t\t\t\t" + anzahlSterne);
        System.out.println("Anzhahl der Bewohner in Berlin:\t" + bewohnerBerlin);
        System.out.println("Mein Alter in Tagen:\t\t\t" + alterTage);
        System.out.println("Mein Gewicht in Kilogram:\t\t" + gewichtKilogramm);
        System.out.println("Flaeche Größtes land:\t\t\t" + flaecheGroessteLand);
        System.out.println("Flaeche kleinstes land:\t\t\t" + flaecheKleinsteLand);

        System.out.println(" *******  Ende des Programms  ******* ");

    }
}

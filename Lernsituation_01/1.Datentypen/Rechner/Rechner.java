import java.util.Scanner;

public class Rechner {

    public static void main(String[] args) // Hier startet das Programm
    {
        // Neues Scanner-Objekt myScanner wird erstellt
        Scanner myScanner = new Scanner(System.in);

        while(true){
            System.out.println("Bitte geben Sie eine ganze Zahl ein: ");

            // Die Variable zahl1 speichert die erste Eingabe
            int zahl1 = myScanner.nextInt();

            System.out.print("Bitte geben Sie eine zweite ganze Zahl ein: ");

            // Die Variable zahl2 speichert die zweite Eingabe
            int zahl2 = myScanner.nextInt();

            System.out.println("Welche Operation?\t+ - * / ");
            char operation = myScanner.next().charAt(0);

            switch (operation){
                case ('+'):
                    System.out.println("Ergebnis der Addition lautet: " + zahl1 + " + " + zahl2 + " = " + (zahl1 + zahl2));
                    break;

                case ('-'):
                    System.out.println("Ergebnis der Addition lautet: " + zahl1 + " - " + zahl2 + " = " + (zahl1 - zahl2));
                    break;

                case ('*'):
                    System.out.println("Ergebnis der Addition lautet: " + zahl1 + " * " + zahl2 + " = " + (zahl1 * zahl2));
                    break;

                case ('/'):
                    System.out.println("Ergebnis der Addition lautet: " + zahl1 + " / " + zahl2 + " = " + ((float)zahl1 / zahl2));
                    break;

                default:
                    System.out.println("Keine Gueltige Operation");
            }
            System.out.println("quit y/n");
            if (myScanner.next().charAt(0) == 'y') {
                break;
            }
        }
        myScanner.close();

    }
}
public class A5_2_2 {
    public static void main(String[] args) {
        int[] intArr = new int[10];
        for (int i = 0; i < intArr.length; i++) {
            intArr[i]= 2*i+1;
        }
        for (int i : intArr) {
            System.out.println(i);
        }
    }
}
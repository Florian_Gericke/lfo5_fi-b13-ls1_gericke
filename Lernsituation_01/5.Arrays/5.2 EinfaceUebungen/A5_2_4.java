public class A5_2_4 {
    public static void main(String[] args) {
        int[] lottoNumbers = {3, 7, 12, 18, 37, 42};

        boolean twelveIsIn = false;
        boolean thirteenIsIn = false;

        for (int i: lottoNumbers) {
            if(i == 12)
                twelveIsIn = true;

            if(i == 13)
                thirteenIsIn = true;

        }

        if(twelveIsIn)
            System.out.println("Die Zahl 12 ist in der Ziehung enthalten.");
        else
            System.out.println("Die Zahl 12 ist nicht in der Ziehung enthalten.");

        if(thirteenIsIn)
            System.out.println("Die Zahl 13 ist in der Ziehung enthalten.");
        else
            System.out.println("Die Zahl 13 ist nicht in der Ziehung enthalten.");
    }
}

import java.util.Arrays;
import java.util.Scanner;

public class ArrayHelper {

//    Aufgabe 1
    public static String convertArrayToString(int[] zahlen){return Arrays.toString(zahlen);}

//    Aufgabe 2
    public static void reverse(int[] arr){
        for (int i = 0; i < arr.length/2; i++) {
            int temp =  arr[i];
            arr[i] = arr[arr.length-1-i];
            arr[arr.length-1-i] = temp;
        }
    }
//    Aufgabe 3
    public static int[] reverseClone(int[] arr){
        int[] re = arr.clone();
        reverse(re);
        return re;
    }

//    Aufgabe 4
    private static double[][] readTemperature(int amount){
        double[][] re = new double[amount][2];
        Scanner sc = new Scanner(System.in);
        for (int i = 0; i < amount; i++) {
            System.out.print("readTemperature in F: ");
            double value = sc.nextDouble();
            re[i][0] = value;
            re[i][1] = ((5.0/9) * (value - 32));
        }
        return re;
    }

//  Aufgabe 5
    private static int[][] readMatrix(int m, int n){
        Scanner sc = new Scanner(System.in);
        int[][] re = new int[n][m];
       for (int i = 0; i < m; i++){
           String[] line = sc.nextLine().split(" ");
           if (line.length != n){
               return null;
           }
           for (int j = 0; j <n ; j++) {
               re[i][j] = Integer.parseInt(line[j]);
           }
       }
        return re;
    }

    //  Aufgabe 6
    public static boolean isTransposed(int[][] one, int[][] other){
        if (one.length != other.length){return false;}
        if (one[0].length != other[0].length){return false;}

        int[][]  temp = new int[one.length][one.length];  //3 rows and 3 columns

//Code to transpose a matrix
        for(int i=0;i< one.length;i++){
            for(int j=0;j<one[0].length;j++){
                temp[i][j]=one[j][i];
            }
        }

        return Arrays.deepEquals(temp,other);
    }
}
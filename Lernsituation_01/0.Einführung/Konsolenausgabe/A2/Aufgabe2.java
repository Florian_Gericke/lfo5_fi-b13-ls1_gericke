package A2;

public class Aufgabe2 {
    public static void main(String[] args) {
      System.out.println(
              "      *\n" +
              "     ***\n" +
              "    *****\n" +
              "   *******\n" +
              "  *********\n" +
              " ***********\n" +
              "*************\n" +
              "     ***\n" +
              "     ***");
    }
}

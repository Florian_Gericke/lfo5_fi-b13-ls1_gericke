import java.util.Scanner;

public class ln2 {
    public static void main(String[] args) {
        System.out.print("Genauigkeit : ");
        int n = new Scanner(System.in).nextInt();
        System.out.println(n);

        double solution = 1;

        for (int i = 2; i < n ; i++) {
             if (i % 2 == 0){
                 solution -= (1.0/i);
             }else{
                 solution += (1.0/i);
             }
        }
        System.out.println("nach a)");
        System.out.println(solution);

        double pos = 0;
        double neg = 0;

        for (int i = 1; i < n ; i++) {
            if (i % 2 == 0){
                neg += (1.0/i);
            }else{
                pos += (1.0/i);
            }
        }
        System.out.println("nach b)");
        System.out.println(pos - neg);
    }
}

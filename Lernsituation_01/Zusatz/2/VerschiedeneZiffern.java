import java.util.Arrays;
import java.util.Scanner;

public class VerschiedeneZiffern {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String input = sc.next();

        for (char c: input.toCharArray()){
            if (input.split(Character.toString(c)).length > 2){
                System.out.println("false");
                return;
            }
        }
        System.out.println("true");
    }
}

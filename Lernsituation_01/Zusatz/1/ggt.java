import java.util.Scanner;

public class ggt {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("ggt var 1: ");
        int a = sc.nextInt();
        System.out.println("ggt var 2: ");
        int b = sc.nextInt();
        System.out.println(ggT(a, b));
    }

    public static int ggT(int a, int b) {
        int ggT = 1;
        int r;
        do {
            r = a % b;
            a = b;
            b = r;
        } while (b != 0);
        return a;
    }
}

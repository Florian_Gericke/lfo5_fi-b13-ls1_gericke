public class A4 {
    public static void main(String[] args) {
        System.out.println(ggT(20, 100));
    }

    public static int ggT(int a, int b) {
        int ggT = 1;
        int r;
        do {
            r = a % b;
            a = b;
            b = r;
        } while (b != 0);
        return a;
    }
}

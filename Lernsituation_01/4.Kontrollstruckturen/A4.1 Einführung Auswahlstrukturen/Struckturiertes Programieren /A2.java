import java.util.Arrays;

public class A2 {
    public static void main(String[] args) {
        printCorrectOrder(3, 2, 6);
    }

    private static void printCorrectOrder(int a, int b, int c) {
        int[] i = {a, b, c};
        Arrays.sort(i);
        for (int ii : i) {
            System.out.println(ii);
        }
    }

}

import java.util.Scanner;

public class Rom {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Roemische Zeichen sind  I, V, X, L, C, D, M");
        char mark = sc.next().toLowerCase().charAt(0);
        String res = "";
        switch (mark) {
            case 'i':
                res = "1";
                break;
            case 'v':
                res = "5";
                break;
            case 'x':
                res = "10";
                break;
            case 'l':
                res = "50";
                break;
            case 'c':
                res = "100";
                break;
            case 'd':
                res = "500";
                break;
            case 'm':
                res = "1000";
                break;
            default:
                res = "Roemische Zeichen sind  I, V, X, L, C, D, M";
                break;
        }
        System.out.println(res);
    }
}

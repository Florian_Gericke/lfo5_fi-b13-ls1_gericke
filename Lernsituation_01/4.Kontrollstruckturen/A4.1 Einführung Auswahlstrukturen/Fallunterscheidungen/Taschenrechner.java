import java.util.Scanner;

public class Taschenrechner {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        System.out.print("Wert 1: ");
        double valOne = sc.nextDouble();
        System.out.print("Wert 2: ");
        double valTwo = sc.nextDouble();
        System.out.print("Operation: ");
        char op = sc.next().charAt(0);


        switch (op) {
            case '+':
                System.out.println(valOne + " + " + valTwo + " = " + (valOne + valTwo));
                break;
            case '-':
                System.out.println(valOne + " - " + valTwo + " = " + (valOne - valTwo));
                break;
            case '*':
                System.out.println(valOne + " * " + valTwo + " = " + (valOne * valTwo));
                break;
            case '/':
                System.out.println(valOne + " / " + valTwo + " = " + (valOne / valTwo));
                break;
            default:
                System.out.println("Erlaubte Zeichen sind +, -, *, /");

        }


    }
}

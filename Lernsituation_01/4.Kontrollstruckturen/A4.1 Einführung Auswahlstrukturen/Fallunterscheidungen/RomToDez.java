import java.util.Scanner;

public class RomToDez {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int res;
        String in = "";
        do {
            System.out.print("Bitte geben sie eine Gueltige Roemische Zahl an: ");
            in = sc.next();
            res = romToDez(in);
            if (res == -2) {
                System.out.println("Fehler in der Umwandlung eins Zeichens");
            }
        } while (res < 0);


        System.out.println(in + " Ist in Dezimal " + res);

    }

    private static boolean checkRom(String rom) {
        boolean re = true;

        char[] cArr = rom.toLowerCase().toCharArray();

        if (tripleAvail(cArr, 'i') || tripleAvail(cArr, 'x') || tripleAvail(cArr, 'c') || tripleAvail(cArr, 'm')) {
            return false;
        }

        if (doubleAvail(cArr, 'v') || doubleAvail(cArr, 'l') || doubleAvail(cArr, 'd')) {
            return false;
        }

        for (int i = 0; i < cArr.length - 1; i++) {
            if (cArr[i] == 'i' && (cArr[i + 1] == 'l' || cArr[i + 1] == 'c' || cArr[i + 1] == 'd' || cArr[i + 1] == 'm')) {
                return false;
            }
            if (cArr[i] == 'x' && (cArr[i + 1] == 'd' || cArr[i + 1] == 'm')) {
                return false;
            }
        }
        return true;
    }

    private static boolean tripleAvail(char[] cArr, char s) {
        for (int i = 0; i < cArr.length - 3; i++) {
            if (cArr[i] == s && cArr[i + 1] == s && cArr[i + 2] == s && cArr[i + 3] == s) {
                return true;
            }
        }
        return false;
    }

    private static boolean doubleAvail(char[] cArr, char s) {
        for (int i = 0; i < cArr.length - 1; i++) {
            if (cArr[i] == s && cArr[i + 1] == s) {
                return true;
            }
        }
        return false;
    }

    private static int romToDez(String rom) {
        if (!checkRom(rom)) {
            return -1;
        }
        int[] arr = new int[rom.length()];
        for (int i = 0; i < rom.length(); i++) {
            arr[i] = getValue(rom.toLowerCase().charAt(i));
            if (arr[i] == -1) {
                return -2;
            }
        }

        for (int i = 0; i < arr.length - 1; i++) {
            if (arr[i] < arr[i + 1]) {
                arr[i] = arr[i + 1] - arr[i];
                arr[i + 1] = 0;
            }
        }

        int res = 0;
        for (int i : arr) {
            res += i;
        }

        return res;
    }


    private static int getValue(char c) {
        switch (c) {
            case 'i':
                return 1;
            case 'v':
                return 5;
            case 'x':
                return 10;
            case 'l':
                return 50;
            case 'c':
                return 100;
            case 'd':
                return 500;
            case 'm':
                return 1000;
            default:
                return -1;
        }
    }

    private static void printSomeRomeNumbers() {
        System.out.println(romToDez("I"));
        System.out.println(romToDez("II"));
        System.out.println(romToDez("III"));
        System.out.println(romToDez("IV"));
        System.out.println(romToDez("V"));
        System.out.println(romToDez("VI"));
        System.out.println(romToDez("VII"));
        System.out.println(romToDez("VIII"));
        System.out.println(romToDez("IX"));
        System.out.println(romToDez("X"));
        System.out.println(romToDez("XI"));
        System.out.println(romToDez("XII"));
        System.out.println(romToDez("XIII"));
        System.out.println(romToDez("XIV"));
        System.out.println(romToDez("XV"));
        System.out.println(romToDez("XVI"));
        System.out.println(romToDez("XVII"));
        System.out.println(romToDez("XVIII"));
        System.out.println(romToDez("XIX"));
        System.out.println(romToDez("XX"));
        System.out.println(romToDez("XXI"));
        System.out.println(romToDez("XXII"));
        System.out.println(romToDez("XXIII"));
        System.out.println(romToDez("XXIV"));
        System.out.println(romToDez("XXV"));
        System.out.println(romToDez("XXVI"));
        System.out.println(romToDez("XXVII"));
        System.out.println(romToDez("XXVIII"));
        System.out.println(romToDez("XXIX"));
        System.out.println(romToDez("XXX"));
        System.out.println(romToDez("L"));
        System.out.println(romToDez("LI"));
        System.out.println(romToDez("LII"));
        System.out.println(romToDez("LIII"));
        System.out.println(romToDez("LIV"));
        System.out.println(romToDez("LV"));
        System.out.println(romToDez("LVI"));
        System.out.println(romToDez("LVII"));
        System.out.println(romToDez("LVIII"));
        System.out.println(romToDez("LIX"));
        System.out.println(romToDez("LX"));
        System.out.println(romToDez("DL"));
        System.out.println(romToDez("DLI"));
        System.out.println(romToDez("DLII"));
        System.out.println(romToDez("DLIII"));
        System.out.println(romToDez("DLIV"));
        System.out.println(romToDez("DLV"));
        System.out.println(romToDez("DLVI"));
        System.out.println(romToDez("DLVII"));
        System.out.println(romToDez("DLVIII"));
        System.out.println(romToDez("DLIX"));
        System.out.println(romToDez("DLX"));
        System.out.println(romToDez("MDL"));
        System.out.println(romToDez("MDLI"));
        System.out.println(romToDez("MDLII"));
        System.out.println(romToDez("MDLIII"));
        System.out.println(romToDez("MDLIV"));
        System.out.println(romToDez("MDLV"));
        System.out.println(romToDez("MDLVI"));
        System.out.println(romToDez("MDLVII"));
        System.out.println(romToDez("MDLVIII"));
        System.out.println(romToDez("MDLIX"));
        System.out.println(romToDez("MDLX"));
    }


}

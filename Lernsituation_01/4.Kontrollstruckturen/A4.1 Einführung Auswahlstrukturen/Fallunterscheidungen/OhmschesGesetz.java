import java.util.Scanner;

public class OhmschesGesetz {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        char unit;
        do {
            System.out.print("Welche Groese Soll berechnet werden R,U,I ?: ");
            unit = sc.next().toLowerCase().charAt(0);
        } while (unit != 'r' && unit != 'u' && unit != 'i');

        if (unit == 'r') {
            System.out.println("R = U/I");
            System.out.print("U = ");
            double u = sc.nextDouble();
            System.out.print("I = ");
            double i = sc.nextDouble();
            System.out.println(u + "V /" + i + "A = " + (u / i) + "\u03A9");
        }
        if (unit == 'u') {
            System.out.println("U = R*I");
            System.out.print("R = ");
            double r = sc.nextDouble();
            System.out.print("I = ");
            double i = sc.nextDouble();
            System.out.println(r + "\u03A9 * " + i + "A = " + (r * i) + "V");
        }
        if (unit == 'i') {
            System.out.println("U = U/R");
            System.out.print("U = ");
            double u = sc.nextDouble();
            System.out.print("R = ");
            double r = sc.nextDouble();
            System.out.println(u + "V / " + r + "\u03A9 = " + (u / r) + "A");
        }

    }
}

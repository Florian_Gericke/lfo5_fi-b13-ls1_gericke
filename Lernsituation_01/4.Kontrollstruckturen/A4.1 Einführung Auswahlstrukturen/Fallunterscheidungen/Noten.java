import java.util.Scanner;

public class Noten {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Gebe eine Note von 1 - 6 ein");
        int mark = sc.nextInt();
        String res = "";
        switch (mark) {
            case 1:
                res = "1 = Sehr Gut";
                break;
            case 2:
                res = "2 = Gut";
                break;
            case 3:
                res = "3 = Befriediegend";
                break;
            case 4:
                res = "4 = Ausreichend";
                break;
            case 5:
                res = "5 = Mangelhaft";
                break;
            case 6:
                res = "6 = Ungenuegend";
                break;
            default:
                res = "Noten gehen nur von 1 - 6";
                break;
        }
        System.out.println(res);
    }
}

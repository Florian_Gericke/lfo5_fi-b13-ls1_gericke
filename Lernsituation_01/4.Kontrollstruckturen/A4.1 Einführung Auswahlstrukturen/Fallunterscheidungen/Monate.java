import java.util.Scanner;

public class Monate {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Gebe eine Note von 1 - 6 ein");
        int mark = sc.nextInt();
        String res;
        switch (mark) {
            case 1:
                res = "1 = Januar";
                break;
            case 2:
                res = "2 = Februar";
                break;
            case 3:
                res = "3 = Maerz";
                break;
            case 4:
                res = "4 = April";
                break;
            case 5:
                res = "5 = Mai";
                break;
            case 6:
                res = "6 = Juni";
                break;
            case 7:
                res = "1 = Juli";
                break;
            case 8:
                res = "2 = August";
                break;
            case 9:
                res = "3 = September";
                break;
            case 10:
                res = "4 = Oktober";
                break;
            case 11:
                res = "5 = November";
                break;
            case 12:
                res = "6 = Dezember";
                break;
            default:
                res = "Monate gehen nur von 1 - 12";
                break;
        }
        System.out.println(res);
    }
}

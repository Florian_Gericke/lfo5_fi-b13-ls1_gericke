import java.util.Scanner;

public class A5_BMI {
    public static void main(String[] args) {
        char sex;
        double wight;
        double height;

        Scanner sc = new Scanner(System.in);

        do {
            System.out.println("Geschlecht m/w");
            sex = sc.next().toLowerCase().charAt(0);
        } while (sex != 'm' && sex != 'w');

        System.out.println("Ihr Gewicht in Kilo");
        wight = sc.nextInt();

        System.out.println("Ihre Größe in Centimeter");
        height = sc.nextDouble()/100;

        double bmi = bmi(wight, height);

        if (sex == 'm') {
            if (bmi < 20) {
                System.out.printf("Ein bmi von %3.2f ist Untergewichtig für Männer\n", bmi);
            }
            if (bmi >= 20 && bmi <= 25) {
                System.out.printf("Ein bmi von %3.2f ist Normal für Männer\n", bmi);
            }
            if (bmi >= 25) {
                System.out.printf("Ein bmi von  %3.2f  ist Übergewichtig für Männer\n", bmi);
            }
        } else {
            if (bmi < 19) {
                System.out.printf("Ein bmi von  %3.2f  ist Untergewichtig für Frauen\n", bmi);
            }
            if (bmi >= 19 && bmi <= 24) {
                System.out.printf("Ein bmi von  %3.2f  ist Normal für Frauen\n", bmi);
            }
            if (bmi >= 24) {
                System.out.printf("Ein bmi von  %3.2f  ist Übergewichtig für Frauen\n", bmi);
            }
        }
    }

    private static double bmi(double wight, double height) {
        height *= height;
        return wight / height;
    }

}

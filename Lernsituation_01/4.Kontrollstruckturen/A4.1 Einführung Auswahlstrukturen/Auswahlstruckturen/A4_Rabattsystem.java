import java.util.Scanner;

public class A4_Rabattsystem {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Einkaufspreis");
        double price = sc.nextDouble();

        if (price >= 0 && price <= 100) {
            price = rabatt(price, 10);
        } else if (price > 100 && price <= 500) {
            price = rabatt(price, 15);
        } else {
            price = rabatt(price, 20);
        }

        System.out.println("Ihr Preis mit Rabatt " + price);
    }

    private static double rabatt(double price, double perc) {
        return price - ((price / 100) * perc);
    }
}

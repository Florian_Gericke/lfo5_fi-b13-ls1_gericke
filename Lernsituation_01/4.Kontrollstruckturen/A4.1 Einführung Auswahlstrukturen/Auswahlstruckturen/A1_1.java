import java.util.Scanner;

public class A1_1 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        System.out.println("Zahl 1: ");
        int firstNum = sc.nextInt();
        System.out.println("Zahl 2: ");
        int secNum = sc.nextInt();

        if (firstNum == secNum) {
            System.out.println("Erste Zahl gleich Zweiter Zahl");
        } else if (secNum > firstNum) {
            System.out.println("Zweite Zahl groeser");
        } else if (firstNum >= secNum) {
            System.out.println("Erste Zahl groeser gleich");
        }

    }
}

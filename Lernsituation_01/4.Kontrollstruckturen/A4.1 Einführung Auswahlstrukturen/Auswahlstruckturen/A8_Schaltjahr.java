import java.util.Scanner;

public class A8_Schaltjahr {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Zu Pruefendes jahr");
        int year = sc.nextInt();
        System.out.println("Das Jahr " + year + " ist " + (isSchaltjahr(year) ? "ein" : "kein") + " Schaltjar");
    }

    private static boolean isSchaltjahr(int year) {

        if (year % 4 == 0) {
            if (year % 100 == 0) {
                return year % 400 == 0;
            }
            return true;
        }
        return false;
    }
}

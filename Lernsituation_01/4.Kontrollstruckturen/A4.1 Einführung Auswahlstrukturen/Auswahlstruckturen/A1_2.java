import java.util.Arrays;
import java.util.Scanner;

public class A1_2 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        System.out.println("Zahl 1: ");
        int firstNum = sc.nextInt();
        System.out.println("Zahl 2: ");
        int secNum = sc.nextInt();
        System.out.println("Zahl 3: ");
        int thrNum = sc.nextInt();

        if (firstNum >= secNum && firstNum >= thrNum) {
            System.out.println("Erste Zahl groeser Zweiter und dritter Zahl");
        } else if (thrNum >= secNum || thrNum >= firstNum) {
            System.out.println("3. groeser 1. oder 2.");
        }

        int[] i = {firstNum, secNum, thrNum};
        Arrays.sort(i);
        for (int ii : i) {
            System.out.println(ii);
        }
    }
}

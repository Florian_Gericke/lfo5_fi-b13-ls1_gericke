import java.util.Scanner;

public class A3_HardwareGrohaendler {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Einzelpreis");
        double price = sc.nextDouble();
        System.out.println("Anzahl");
        double amount = sc.nextInt();
        char in;
        do {
            System.out.println("Ermaeziegt j/n");
            in = sc.next().toLowerCase().charAt(0);
        } while (in != 'j' && in != 'n');

        price *= amount;

        price += (amount < 10) ? 10 : 0;

        System.out.println("Ihr Nette Betrag " + toNetto(price, in));
    }

    private static double toNetto(double amount, char type) {
        double tax = (type == 'j') ? 7 : 19;
        tax = (amount / 100) * tax;
        return amount + tax;
    }
}

import java.util.Scanner;

public class A6_Funktionsloeser {
    private static final double EULA = 2.718;

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        double x = sc.nextDouble();

        if (x <= 0) {
            System.out.println("f(" + x + ") = e^" + x + " = " + (Math.pow(EULA, x)) + " exponentiell");
        }
        if (x > 0 && x <= 3) {
            System.out.println("f(" + x + ") = " + x + "^2 +1 = " + (Math.pow(x, 2) + 1) + " quadratisch");
        }
        if (x > 3) {
            System.out.println("f(" + x + ") = 2*" + x + "+4 = " + (2 * x + 4) + " linear");
        }
    }
}

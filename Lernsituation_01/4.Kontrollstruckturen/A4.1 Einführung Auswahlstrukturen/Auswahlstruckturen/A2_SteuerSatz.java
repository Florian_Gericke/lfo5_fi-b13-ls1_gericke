import java.util.Scanner;

public class A2_SteuerSatz {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Brutto Betrag");
        double amount = sc.nextDouble();
        char in;
        do {
            System.out.println("Ermaeziegt j/n");
            in = sc.next().toLowerCase().charAt(0);
        } while (in != 'j' && in != 'n');

        System.out.println("Ihr Nette Betrag " + toNetto(amount, in));
    }

    private static double toNetto(double amount, char type) {
        double tax = (type == 'j') ? 7 : 19;
        tax = (amount / 100) * tax;
        return amount + tax;
    }
}

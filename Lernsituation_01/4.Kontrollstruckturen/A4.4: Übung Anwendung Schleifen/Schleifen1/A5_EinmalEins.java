public class A5_EinmalEins {
    public static void main(String[] args) {
        for (int x = 1; x <= 10 ; x++) {
            String line = "";
            for (int y = 1; y <= 10 ; y++) {
                line += " " + ((x*y < 10) ? (" "+x*y) : (x*y) );
            }
            System.out.println(line);
        }
    }
}

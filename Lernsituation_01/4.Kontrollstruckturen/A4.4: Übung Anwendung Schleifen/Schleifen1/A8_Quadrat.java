import java.util.Scanner;

public class A8_Quadrat {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Wie gros soll eine seite werden? ");
        int original = sc.nextInt();
        int max = original;
        while (0 != max--){
            System.out.print("* ");
            int width = original;
            while (2 < width--){
                if (max == original-1 || max == 0){
                    System.out.print("* ");
                }else{
                    System.out.print("  ");
                }
            }
            System.out.println("*");
        }

    }
}

public class A3_Modulo {
    public static void main(String[] args) {
        for (int i = 1; i <=200 ; i++) {
            if (i % 7 == 0) {
                System.out.println(i + " ist teilbar durch 7");
            }
            if (i % 5 != 0 && i % 4==0) {
                System.out.println(i + " ist nicht teilbar 5 aber teilbar 4");
            }
        }
    }
}

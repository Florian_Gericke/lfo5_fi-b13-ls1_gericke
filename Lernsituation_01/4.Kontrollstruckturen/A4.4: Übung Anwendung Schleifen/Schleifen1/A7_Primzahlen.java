import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

public class A7_Primzahlen {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Geben sie eine Ganze Zahl an: ");
        System.out.println(erathos(sc.nextInt()));
    }

    private static String erathos(int max){
        boolean[] isprim = new boolean[max];
        for (int i = 2; i < isprim.length; i++) {
            isprim[i]= true;
        }

        for (int i = 2; i < max; i++) {
            for (int j = 2*i; j < max ; j+=i) {
                isprim[j] = false;
            }
        }
        ArrayList<Integer>  list = new ArrayList<>();
        for (int i = 0; i < isprim.length; i++) {
            if (isprim[i]){
                list.add(i);
            }
        }
        return Arrays.toString(list.toArray());
    }

}

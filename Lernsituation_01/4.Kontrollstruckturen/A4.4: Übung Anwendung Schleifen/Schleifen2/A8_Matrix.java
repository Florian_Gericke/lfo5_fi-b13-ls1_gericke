import java.util.Scanner;

public class A8_Matrix {
    public static void main(String[] args) {
        int n;
        Scanner sc = new Scanner(System.in);

        do{
            System.out.print("Geben sie eine Zahl zwischen 2 und 9 ein: ");
            n = sc.nextInt();
        }while(n < 2 || n > 9);

        for (int i = 1; i < 100 ; i++) {
            if ((Integer.toString(i).indexOf(Integer.toString(n).charAt(0)) >= 0) || (quer(i) == n) ||(i % n == 0)){
                System.out.print("  *");
            }else{
                System.out.print(i < 10 ? "  "+i:" "+i);
            }
            if ((i+1 )% 10 == 0){
                System.out.println();
            }
        }
    }
    private static int quer(int i){
        int re = 0;
        while(i > 10){
            re += i % 10;
            i /= 10;
        }
        re += i;
        return re;
    }
}

import java.util.Scanner;

public class A5_Zinseszins {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        do{
            System.out.print("Ihre einmalig Einlage: ");
            double einlage = sc.nextDouble();

            System.out.print("Ihre Zinssatz: ");
            double zinsen = sc.nextDouble();

            int years = 0;

            while (einlage < 1000000){
                years++;
                einlage += getPercent(einlage, zinsen);
            }

            System.out.println("Sie sind Milionär in "+ years + " Jahren\n Neuer Durchlauf ? (y)");
        }while (sc.next().toLowerCase().charAt(0) == 'y');


    }

    private static double getPercent(double value ,double percent){
        return (value/100)*percent;
    }

}

import java.util.Scanner;

public class A2_fak {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Fakultaet von: ");
        System.out.println(fak(sc.nextInt()));
    }
    public static int fak(int n) {
        return n == 1 ? n : n * fak(--n);
    }
}

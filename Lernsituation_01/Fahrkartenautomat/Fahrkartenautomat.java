import java.lang.reflect.Array;
import java.util.Locale;
import java.util.Scanner;

class Fahrkartenautomat {
    public static void main(String[] args) {



        double preisTicket;
        double eingezahlterGesamtbetrag;
        double rueckgabebetrag;
        int anzahlTickets;

        // Endlos Modus
        while(true){
            preisTicket = fahrkartenbestellungErfassen();

            // Geldeinwurf
            // -----------
            eingezahlterGesamtbetrag = fahrkartenBezahlen(preisTicket);

            // Fahrscheinausgabe
            // -----------------
            fahrkartenAusgeben();

            // Rückgeldberechnung und -Ausgabe
            // -------------------------------
            rueckgeldAusgeben(eingezahlterGesamtbetrag - preisTicket);
        }
    }


    private static double fahrkartenbestellungErfassen() {
        String[] type ={
                "Einzelfahrschein Berlin AB",
                "Einzelfahrschein Berlin BC",
                "Einzelfahrschein Berlin ABC",
                "Kurzstrecke",
                "Tageskarte Berlin AB",
                "Tageskarte Berlin BC",
                "Tageskarte Berlin ABC",
                "Kleingruppen-Tageskarte Berlin AB",
                "Kleingruppen-Tageskarte Berlin BC",
                "Kleingruppen-Tageskarte Berlin ABC"
        };
        double[] price = {
                2.90,
                3.30,
                3.60,
                1.90,
                8.60,
                9.00,
                9.60,
                23.50,
                24.30,
                24.90
        };
        double preis = 0;
        String in;
        int amount = 0;
        Scanner tastatur = new Scanner(System.in);
        do{
            for (int i = 0; i < type.length; i++) {
                System.out.println(i+1 +  " " + type[i] + " " + price[i]);
            }
            System.out.println("Bezahlen (p)");
            System.out.println("beenden (q)");
            in = tastatur.next();

            if(in.toLowerCase().equals("p")){
                break;
            }else if(in.toLowerCase().equals("q")){
                System.exit(0);
            }

            int i = Integer.parseInt(in);

            if(i > 0 && i <= price.length){
                preis += price[i-1];
                amount++;
            }
            System.out.println("\nIhre Wahl ist "+in);
            System.out.println("Anzahl Tickets: "+amount+"\n");
        }while (true);

        return preis;
    }

    private static double fahrkartenBezahlen(double zuZahlen) {
        Scanner tastatur = new Scanner(System.in);
        double eingezahlterGesamtbetrag = 0.0;
        while (eingezahlterGesamtbetrag < zuZahlen) {
            System.out.printf("Noch zu zahlen: %.2f \n", (zuZahlen - eingezahlterGesamtbetrag));
            System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
            double eingeworfeneMuenze = tastatur.nextDouble();
            eingezahlterGesamtbetrag += eingeworfeneMuenze;
        }
        return eingezahlterGesamtbetrag;
    }

    private static void fahrkartenAusgeben(){
        System.out.println("\nFahrschein wird ausgegeben");
        for (int i = 0; i < 8; i++) {
            System.out.print("=");
            try {
                warte(250);
            } catch (InterruptedException e){
                e.printStackTrace();
            }
        }
        System.out.println("\n\n");
    }

    //  durch das throws gebe ich die Exception an die rufende stelle zurueck und lass sie dort abfangen da die Funktion nur das Warten ersetzen soll
    private static void warte(int millisekunde) throws InterruptedException {
        Thread.sleep(250);
    }

    private static void muenzeAusgeben(int value, String symbol) {
        System.out.println(value + " " + symbol);
    }

    private static void rueckgeldAusgeben(double rueckgabebetrag) {
        if (rueckgabebetrag > 0.0) {
            System.out.printf("Der Rückgabebetrag in Höhe von %.2f EURO \n", rueckgabebetrag);
            System.out.println("wird in folgenden Münzen ausgezahlt:");

            while (rueckgabebetrag >= 2.0) // 2 EURO-Münzen
            {
                muenzeAusgeben(2, "\u20AC");
                rueckgabebetrag -= 2.0;
            }
            while (rueckgabebetrag >= 1.0) // 1 EURO-Münzen
            {
                muenzeAusgeben(1, "\u20AC");
                rueckgabebetrag -= 1.0;
            }
            while (rueckgabebetrag >= 0.5) // 50 CENT-Münzen
            {
                muenzeAusgeben(50, "CENT");
                rueckgabebetrag -= 0.5;
            }
            while (rueckgabebetrag >= 0.2) // 20 CENT-Münzen
            {
                muenzeAusgeben(20, "CENT");
                rueckgabebetrag -= 0.2;
            }
            while (rueckgabebetrag >= 0.1) // 10 CENT-Münzen
            {
                muenzeAusgeben(10, "CENT");
                rueckgabebetrag -= 0.1;
            }
            while (rueckgabebetrag >= 0.05)// 5 CENT-Münzen
            {
                muenzeAusgeben(5, "CENT");
                rueckgabebetrag -= 0.05;
            }
        }

        System.out.println("\nVergessen Sie nicht, den Fahrschein\n" +
                "vor Fahrtantritt entwerten zu lassen!\n" +
                "Wir wünschen Ihnen eine gute Fahrt.");
    }
}

// Vorteile
//      - Schnelleres Hinzufuegen von Neuen Fahrkarten
//      - Schnelles Akutualiesieren von Preisen

// Nachteile
//      Die verwendung von 2 Arrays erhoet fehlerpotenzial
//      Beide Arrays muessen gleich gross sein => mehr fehlermoeglichkeiten die Beachtet werden muessen
//                                                => Erhoete Komplexitaet des Codes
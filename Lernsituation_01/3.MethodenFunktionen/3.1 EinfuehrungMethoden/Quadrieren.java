public class Quadrieren {
    public static void main(String[] args) {

        // (E) "Eingabe"
        // Wert für x festlegen:
        // ===========================
        System.out.println("Dieses Programm berechnet die Quadratzahl x²");
        System.out.println("---------------------------------------------");
        double x = 5;

        // (V) Verarbeitung
        // Quadrat von x und y berechnen:
        // ================================
        double ergebnis = quad(x);

        // (A) Ausgabe
        // Ergebnis auf der Konsole ausgeben:
        // =================================
        System.out.printf("x = %.2f und x²= %.2f\n", x, ergebnis);
    }

    private static double quad(double x){return x*x;}

}

import java.util.Scanner;

public class PCHaendler {
    public static void main(String[] args) {
        Scanner myScanner = new Scanner(System.in);

        // Benutzereingaben lesenp
        System.out.println("was moechten Sie bestellen?");
        String artikel = liesString();

        System.out.println("Geben Sie die Anzahl ein:");
        int anzahl = liesInt();

        System.out.println("Geben Sie den Nettopreis ein:");
        double preis = liesDouble();

        System.out.println("Geben Sie den Mehrwertsteuersatz in Prozent ein:");
        double mwst = liesDouble();

        // Verarbeiten
        double nettogesamtpreis =  berechneGesamtnettopreis(anzahl, preis);
        double bruttogesamtpreis = berechneGesamtbruttopreis(nettogesamtpreis, mwst);

        // Ausgeben
        rechungausgeben(artikel,anzahl,nettogesamtpreis,bruttogesamtpreis,mwst);
    }

    public static String liesString() {
        return new Scanner(System.in).next();
    }

    public static int liesInt() {
        return new Scanner(System.in).nextInt();
    }

    public static double liesDouble() {
        return new Scanner(System.in).nextDouble();
    }

    public static double berechneGesamtnettopreis(int anzahl, double nettopreis) {
        return anzahl * nettopreis;
    }

    public static double berechneGesamtbruttopreis(double nettogesamtpreis, double mwst) {
        return nettogesamtpreis * (1 + mwst / 100);
    }

    public static void rechungausgeben(String artikel, int anzahl, double nettogesamtpreis, double bruttogesamtpreis, double mwst) {
        System.out.printf("\tRechnung\n" +
                "\t\t Netto:  %-20s %6d %10.2f \n" +
                "\t\t Brutto: %-20s %6d %10.2f (%.1f%s)", artikel, anzahl, nettogesamtpreis, artikel, anzahl, bruttogesamtpreis, mwst, "%"
        );
    }
}

public class MatheTest {
    public static void main(String[] args) {
        double x1 = 1.732, x2;
        int n = 2;

        x2 = Mathe.berechnePotenz(x1, n);
        System.out.println(x1 + " hoch " + n + " = " + x2);
        System.out.println(Mathe.berechneKreisflaeche(12.0));
    }
}

/*
    Aufgabe 2:

        Die Uebergabe von Argument an eine Funktion funktioniert ueber Parameter.
        Das sind die Variablen die in der Funktionssignatur in den Runden Klammern stehen

     Aufgabe 3:

        Das Vertauschen ueber die Parameter geht nicht mit einfachen Datentypen da diese in java
        "pass by Value" sind und ich als Entwickler keine Moeglichkeit habe sie per Reference zu uebergeben.

        Ausnahmen bieten hier Complexe Datentypen wie Arrays zbs.
 */